import React from 'react';

// Composants de React Router
import { BrowserRouter, Route, Switch } from 'react-router-dom';

// Barre de navigation
import { NavBar }  from './components/NavBar';

// Pages de l'application
import { Basket }  from './pages/Basket';
import { Home }    from './pages/Home';
import { Product } from './pages/Product';

// Contexte de l'application
import { AppContext } from './AppContext';


export class App extends React.Component {

    /*
     * Le state global de l'application, manipulé en utilisant un contexte React.
     *
     * En permettant aux composants enfants dans la hiérarchie d'agir sur ce state au travers du contexte
     * cela permet de profiter du fonctionnement naturel de React de rafraichissement automatique de
     * l'affichage - appel de la méthode render() - après avoir mis à jour le state via setState().
     */
    state = {
        basket: [],


        addToBasket: (productCode) => {

            // Duplication du state contenant le panier.
            let basket = [ ...this.state.basket ];

            // Recherche du produit dans le panier.
            let basketItem = basket.find((basketItem) => basketItem.productCode === productCode);

            // Est-ce que le produit existe déjà dans le panier ?
            if(basketItem === undefined) {
                // Non, ajout initial du produit.
                basket.push({ productCode: productCode, quantity: 1 });
            } else {
                // Oui, mise à jour de la quantité du produit.
                basketItem.quantity++;
            }

            // Mise à jour du panier.
            this.setState({ basket: basket });
        },

        clearBasket: () => {

            // Vidage complet du panier.
            this.setState({ basket: [] });
        }
    };


    render() {

        // Construction de la coquille de l'application avec le contexte, le routeur et l'affichage commun.
        return (
            <AppContext.Provider value={ this.state }>  {/* Contexte de départ de l'application basé sur le state */}
                <BrowserRouter>                         {/* https://reacttraining.com/react-router/web/api/BrowserRouter */}

                    {/* Affichage commun de l'application. */}
                    <header>
                        <NavBar />
                        <h1>Geek Shop</h1>
                    </header>

                    {/* Routing de l'application. */}
                    <Switch>                            {/* https://reacttraining.com/react-router/web/api/Switch */}
                        <Route exact path="/">          {/* https://reacttraining.com/react-router/web/api/Route */}
                            <Home />
                        </Route>
                        <Route exact path="/basket">
                            <Basket />
                        </Route>
                        <Route exact path="/product/:code">
                            <Product />
                        </Route>
                    </Switch>
                </BrowserRouter>
            </AppContext.Provider>
        );
    }
}