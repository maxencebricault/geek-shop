import React from 'react';

import { ProductList } from '../components/ProductList';

import { productDatabase } from '../lib/database';

// Feuille de styles spécifique à cette page (importation via Webpack).
import './home.css';


export function Home() {

    return (
        <main>
            <h2>Accueil</h2>
            <ProductList database={ productDatabase } />
        </main>
    );
}