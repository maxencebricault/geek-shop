import React from 'react';

import { Invoice } from '../components/Invoice';


export function Basket() {

    return (
        <main>
            <h2>Votre panier d'achats</h2>
            <Invoice />
        </main>
    );
}