export const productDatabase = [

    { productCode: 'DBZ', description: 'Dragonball Z Kai - Saga de Boo',   unitPrice: 29.90 },
    { productCode: 'FMA', description: 'Full Metal Alchemist Brotherhood', unitPrice: 19.50 },
    { productCode: 'SKY', description: 'Skyfall',                          unitPrice: 22.50 },
    { productCode: 'OPM', description: 'One Punch Man',                    unitPrice: 25.70 },
    { productCode: 'SWT', description: 'Star Wars épisode V',              unitPrice: 29.90 }
];


export const voucherDatabase = [

    { 'NOEL2020'     : 0.12 },
    { 'ANNIVERSAIRE' : 0.15 },
    { 'SOLDES_ETE'   : 0.25 }
];


export function findProduct(productCode) {

    /*
     * Array.find recherche le premier produit correspondant au code de produit spécifié.
     * Si aucun produit n'est trouvé, renvoie false (via l'opérateur null coalescing).
     *
     * https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Objets_globaux/Array/find
     * https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Operators/Nullish_coalescing_operator
     */
    return productDatabase.find((product) => product.productCode === productCode) ?? false;
}


export function findVoucher(voucherCode) {

    for(const voucher of voucherDatabase) {

        // Est-ce que le code de réduction spécifié correspond ?
        if(voucherCode in voucher) {

            // Oui, retourne le taux de réduction correspondant.
            return voucher[voucherCode];

            /* -- RAPPEL :
             * Syntaxiquement on peut manipuler un objet et ses propriétés comme un tableau associatif :
             * object.property et object[property] veulent dire la même chose
             * 
             * Par exemple dans notre cas on peut écrire autant voucher.NOEL2020 que voucher['NOEL2020']
             */
        }
    }

    return false;
}