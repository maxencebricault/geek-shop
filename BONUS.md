BONUS
=====

```js
const length = context.basket.reduce((accumulator, basketItem) =>
{
    return accumulator + basketItem.quantity;
}, 0);
```

- implémenter local storage
- implémenter clear basket


BIBLIOGRAPHIE
=============

- https://www.alsacreations.com/article/lire/1397-attributs-data-et-api.html
- https://www.alsacreations.com/article/lire/1402-web-storage-localstorage-sessionstorage.html